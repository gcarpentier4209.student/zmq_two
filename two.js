const express = require('express')
const app = express()
const port = 3001

var zmq = require("zeromq"),
sock = zmq.socket("pull");
sock.connect("tcp://127.0.0.1:3002");

var count = 0;
app.get('/', (req, res) => {
  res.send('Hello Two! Test : ' + count)

})

app.listen(port, () => {
  console.log(` listening at http://localhost:${port}`)
  sock.on("message", function(msg) {
    console.log("received: %s", msg.toString());
    count++;
  });
})

